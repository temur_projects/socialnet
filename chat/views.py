from django.shortcuts import render
from django.utils.safestring import mark_safe
import json
from .models import ChatMessage, Room
from account.models import Friend, Profile


def index(request):
    return render(request, 'chat/index.html', {})


def room(request, username, room_name):
    r = Room.objects.get(name=room_name)
    messages = r.messages.all()
    print(r, messages)
    return render(request, 'chat/room.html', {
        'room_name_json': mark_safe(json.dumps(room_name)),
        'user_name_json': mark_safe(json.dumps(username)),

        'messages': messages,
    })


def ChatView(request, username):
    user = request.user
    friends = Profile.objects.get(user=request.user).friends.all() 
    return render(request, 'chat/chatview.html', {'friends': friends})
