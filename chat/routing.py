from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
    url(r'^ws/account/(?P<username>[^/]+)/chat/(?P<room_name>[^/]+)/$', consumers.ChatConsumer),
]