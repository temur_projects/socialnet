from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Room(models.Model):
    name = models.CharField(max_length=250, db_index=True)
    user1 = models.ForeignKey('auth.User', related_name='first', on_delete=models.CASCADE)
    user2 = models.ForeignKey('auth.User', related_name='second', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class ChatMessage(models.Model):

    user = models.ForeignKey('auth.User', related_name='kdf', on_delete=models.CASCADE)
    room = models.ForeignKey(Room, related_name='messages', on_delete=models.CASCADE)
    message = models.TextField(max_length=3000)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        """
        String to represent the message
        """

        return self.message
