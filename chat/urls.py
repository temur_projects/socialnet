from django.urls import path

from . import views
from . import consumers

urlpatterns = [
  
    path('<room_name>/', views.room),
]
