from django.contrib import admin

# Register your models here.
from .models import ChatMessage, Room

admin.site.register(ChatMessage)
admin.site.register(Room)
