from django.db import models
from django.utils import timezone


class Post(models.Model):
    author = models.ForeignKey('auth.user', related_name="posts", on_delete=models.CASCADE)
    images = models.ImageField(upload_to='users/%Y/%m/%d/', blank=True)
    file = models.FileField(upload_to='users/%Y/%m/%d/', blank=True)
    text = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

