from django.shortcuts import render, get_object_or_404, HttpResponse
from .forms import UserRegistrationForm
from django.contrib.auth.decorators import login_required
from post.forms import PostForm
from django.contrib.auth.models import User
from .models import Friend, Profile
from chat.models import Room
import os
import binascii

def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(user_form.cleaned_data['password'])
            new_user.save()
            Profile.objects.create(user=User.objects.get(username=request.user))

            return render(request, 'account/register_done.html', {'new_user': new_user})

    else:
        user_form = UserRegistrationForm()
    return render(request, 'account/register.html', {'user_form': user_form})


@login_required
def dashboard(request, username):
    user = User.objects.get(username=username)
    friend_list = Friend.objects.filter(user_to=user, confirmed=False)
    posts = user.posts.all()
    if request.method == 'POST':
        post_form = PostForm(request.POST, request.FILES)
        if post_form.is_valid():
            new_post = post_form.save(commit=False)
            new_post.author = request.user
            new_post.save()
    else:
        post_form = PostForm()

    return render(request,
                  'account/his_dashboard.html',
                  {'friend_list': friend_list,
                   'request.user': request.user,
                   'user': user,
                   'post_form': post_form,
                   'posts': posts})


'''

@login_required
def user_list(request):
    users = User.objects.filter(is_active=True)
    return render(request,
                  'account/user/list.html',
                  {'section': 'people',
                   'users': users})


@login_required
def user_detail(request, username):
    user = get_object_or_404(User, username=username, is_active=True)
    return render(request,
                  'account/user/detail.html',
                  {'section': 'people',
                   'user': user})
                   '''


@login_required
def friend_request(request, username):
    f = Friend.objects.filter(user_from=request.user, user_to=User.objects.get(username=username))
    if f.exists():
        return HttpResponse('FAIL')
    friends = Friend(user_from=request.user, user_to=User.objects.get(username=username))
    friends.save()

    return HttpResponse('OK')


@login_required
def confirm(request, username):
    user1 = request.POST.get('user1')
    user2 = request.POST.get('user2')
    dat = Friend.objects.get(user_from=User.objects.get(username=user1), user_to=User.objects.get(username=user2))
    Profile.objects.get(user=User.objects.get(username=user1)).friends.add(User.objects.get(username=user2))
    Profile.objects.get(user=User.objects.get(username=user2)).friends.add(User.objects.get(username=user1))

    dat.confirmed = True
    dat.save()
    out = os.urandom(20)
    string = str(binascii.hexlify(out))[2:-1]

    Room.objects.create(user1=User.objects.get(username=user1), user2=User.objects.get(username=user2), name=string)
    return HttpResponse(dat)
