from django.contrib import admin
from .models import Friend, Profile
# Register your models here.
admin.site.register(Friend)
admin.site.register(Profile)