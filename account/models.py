from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField('auth.user', on_delete=models.CASCADE)
    date_of_birth = models.DateField(blank=True, null=True)
    photo = models.ImageField(upload_to='users/%Y/%m/%d/', blank=True)
    friends = models.ManyToManyField('auth.user', related_name='friendship', blank=True)

    def __str__(self):
        return 'Profile for user {}'.format(self.user.username)


class Friend(models.Model):
    user_from = models.ForeignKey(
        'auth.User', related_name='friend_from_set', on_delete=models.CASCADE)
    user_to = models.ForeignKey(
        'auth.User', related_name='friend_to_set', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, db_index=True)
    confirmed = models.BooleanField(default=False)

    def __str__(self):
        if self.confirmed:
            return '{} is a friend of  {}'.format(self.user_from, self.user_to)
        return '{} wants to be a friend with you'.format(self.user_from)


"""
    def save(self, *args, **kwargs):
        if self.confirmed:
            self.confirmed = True
            if self not in Friend.objects.all():
                super(Friend, self).save(*args, **kwargs)
"""


#User.add_to_class('friends', models.ManyToManyField(    'self', through=Friend, related_name='friendship', symmetrical=False))
