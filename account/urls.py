from django.urls import path, include
from django.contrib.auth import views as aviews
from . import views
from chat import views as aview

# app_name='account'

urlpatterns = [
    # previous login view
    # path('login/', views.user_login, name='login'),
    path('login/', aviews.LoginView.as_view(), name='login'),
    path('logout/', aviews.LogoutView.as_view(), name='logout'),
    path('<username>/', views.dashboard, name='dashboard'),
    # change password urls
    path('friend_request/<username>/', views.friend_request, name='friend_request'),
    # path('', include('django.contrib.auth.urls')),
    path('register/', views.register, name='register'),
    path('<username>/confirm/', views.confirm, name='confirm'),

    path('<username>/chat/<room_name>/', aview.room, name='room'),
    path('<username>/chat/', aview.ChatView, name='cv'),

]
